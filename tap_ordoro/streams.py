"""Stream type classes for tap-ordoro."""

from datetime import datetime
from typing import Iterable

import requests
from singer_sdk import typing as th
from singer_sdk.helpers.jsonpath import extract_jsonpath

from tap_ordoro.client import OrdoroStream


class OrdersStream(OrdoroStream):
    """Define custom stream."""

    name = "order"
    path = "/v3/order"
    primary_keys = ["order_number"]
    replication_key = "updated_date"
    records_jsonpath = "$.order[*]"

    schema = th.PropertiesList(
        th.Property("order_number", th.StringType),
        th.Property("barcode", th.StringType),
        th.Property("order_placed_date", th.DateTimeType),
        th.Property("created_date", th.DateTimeType),
        th.Property("updated_date", th.DateTimeType),
        th.Property("cancelled_date", th.DateTimeType),
        th.Property("status", th.DateTimeType),
        th.Property("shippability", th.StringType),
        th.Property(
            "shipping_address",
            th.ObjectType(
                th.Property("name", th.StringType),
                th.Property("company", th.StringType),
                th.Property("street1", th.StringType),
                th.Property("street2", th.StringType),
                th.Property("city", th.StringType),
                th.Property("state", th.StringType),
                th.Property("zip", th.StringType),
                th.Property("country", th.StringType),
                th.Property("email", th.StringType),
                th.Property("phone", th.StringType),
                th.Property(
                    "validation",
                    th.ObjectType(
                        th.Property("status", th.StringType),
                        th.Property("additional_text", th.StringType),
                        th.Property(
                            "suggested", th.CustomType({"type": ["array", "string"]})
                        ),
                    ),
                ),
            ),
        ),
        th.Property(
            "billing_address",
            th.ObjectType(
                th.Property("name", th.StringType),
                th.Property("company", th.StringType),
                th.Property("street1", th.StringType),
                th.Property("street2", th.StringType),
                th.Property("city", th.StringType),
                th.Property("state", th.StringType),
                th.Property("zip", th.StringType),
                th.Property("country", th.StringType),
                th.Property("email", th.StringType),
                th.Property("phone", th.StringType),
                th.Property(
                    "validation",
                    th.ObjectType(
                        th.Property("status", th.StringType),
                        th.Property("additional_text", th.StringType),
                        th.Property(
                            "suggested", th.CustomType({"type": ["array", "string"]})
                        ),
                    ),
                ),
            ),
        ),
        th.Property("label_infos", th.CustomType({"type": ["array", "number"]})),
        th.Property(
            "lines",
            th.ArrayType(
                th.ObjectType(
                    th.Property("link", th.StringType),
                    th.Property("id", th.NumberType),
                    th.Property("quantity", th.NumberType),
                    th.Property("item_price", th.NumberType),
                    th.Property("total_price", th.NumberType),
                    th.Property("discount_amount", th.NumberType),
                    th.Property("supplier_price", th.NumberType),
                    th.Property("sku", th.StringType),
                    th.Property("product_name", th.StringType),
                    th.Property(
                        "product_serial_numbers",
                        th.CustomType({"type": ["array", "string"]}),
                    ),
                    th.Property("product_is_kit_parent", th.BooleanType),
                    th.Property("order_line_product_name", th.StringType),
                    th.Property("product_link", th.StringType),
                    th.Property("product_category", th.StringType),
                    th.Property("cart_order_id", th.StringType),
                    th.Property("cart_orderitem_id", th.StringType),
                    th.Property("cogs", th.NumberType),
                    th.Property(
                        "shippability",
                        th.ObjectType(
                            th.Property("shippability", th.StringType),
                            th.Property("supplier_id", th.NumberType),
                            th.Property("is_dropship", th.BooleanType),
                        ),
                    ),
                    th.Property("details", th.StringType),
                    th.Property("upc", th.StringType),
                    th.Property("sales_channel_location_id", th.StringType),
                )
            ),
        ),
        th.Property("weight", th.NumberType),
        th.Property("notes_from_customer", th.StringType),
        th.Property("internal_notes", th.StringType),
        th.Property("requested_shipping_method", th.StringType),
        th.Property("deliver_by_date", th.DateTimeType),
        th.Property(
            "sales_channel",
            th.ObjectType(
                th.Property("id", th.NumberType),
                th.Property("link", th.StringType),
            ),
        ),
        th.Property(
            "warehouse",
            th.ObjectType(
                th.Property("id", th.NumberType),
                th.Property("link", th.StringType),
            ),
        ),
        th.Property("shipping_info", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "return_shipping_info", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property("dropshipping_info", th.CustomType({"type": ["object", "string"]})),
        th.Property(
            "comments",
            th.ArrayType(
                th.ObjectType(
                    th.Property("user", th.StringType),
                    th.Property("date", th.DateTimeType),
                    th.Property("text", th.StringType),
                )
            ),
        ),
        th.Property("tags", th.CustomType({"type": ["array", "string"]})),
        th.Property(
            "financial",
            th.ObjectType(
                th.Property("credit_card_issuer", th.StringType),
                th.Property("discount_amount", th.NumberType),
                th.Property("product_amount", th.NumberType),
                th.Property("shipping_amount", th.NumberType),
                th.Property("tax_amount", th.NumberType),
                th.Property("grand_total", th.NumberType),
            ),
        ),
        th.Property("link", th.StringType),
        th.Property(
            "additional_cart_info", th.CustomType({"type": ["object", "string"]})
        ),
        th.Property("is_order_parent", th.BooleanType),
        th.Property("parent_order_number", th.StringType),
        th.Property(
            "sibling_order_numbers", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("audit_label", th.CustomType({"type": ["array", "string"]})),
        th.Property(
            "return_order_reference_ids", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("similar_open_addresses_count", th.NumberType),
        th.Property("batch_reference_id", th.StringType),
    ).to_dict()

    def parse_response(self, response: requests.Response) -> Iterable[dict]:
        """Parse the response and return an iterator of result rows."""
        if "replication_key_value" in self.stream_state:
            for record in extract_jsonpath(
                self.records_jsonpath, input=response.json()
            ):
                # Get records modified date
                record_mod_date = datetime.strptime(
                    record[self.replication_key].split(".")[0], "%Y-%m-%dT%H:%M:%S"
                )
                # Fetch replication value for last order update_date
                replication_value = datetime.strptime(
                    self.stream_state["replication_key_value"].split(".")[0],
                    "%Y-%m-%dT%H:%M:%S",
                )
                # Return only orders which were updated after replication value
                if record_mod_date > replication_value:
                    yield record
        else:
            yield from extract_jsonpath(self.records_jsonpath, input=response.json())


class ProductsStream(OrdoroStream):
    """Define custom stream."""

    name = "product"
    path = "/product/"
    primary_keys = ["id"]
    replication_key = "updated"
    records_jsonpath = "$.product[*]"
    schema = th.PropertiesList(
        th.Property("type", th.StringType),
        th.Property("cost", th.NumberType),
        th.Property("id", th.NumberType),
        th.Property("company_id", th.NumberType),
        th.Property("name", th.StringType),
        th.Property("sku", th.StringType),
        th.Property("taxable", th.StringType),
        th.Property("weight", th.NumberType),
        th.Property("length", th.NumberType),
        th.Property("width", th.NumberType),
        th.Property("height", th.NumberType),
        th.Property("price", th.NumberType),
        th.Property("customer_text", th.StringType),
        th.Property("internal_notes", th.StringType),
        th.Property("total_on_hand", th.NumberType),
        th.Property("category", th.StringType),
        th.Property("is_kit_parent", th.BooleanType),
        th.Property("archive_date", th.DateTimeType),
        th.Property("hs_code", th.StringType),
        th.Property("country_of_origin", th.StringType),
        th.Property("customs_description", th.StringType),
        th.Property("created", th.DateTimeType),
        th.Property("updated", th.DateTimeType),
        th.Property("upc", th.StringType),
        th.Property("asin", th.StringType),
        th.Property("image_url", th.StringType),
        th.Property("default_image_id", th.IntegerType),
        th.Property("_link", th.StringType),
        th.Property("tags", th.CustomType({"type": ["array", "string"]})),
        th.Property("linked_skus", th.CustomType({"type": ["array", "string"]})),
        th.Property("total_committed", th.NumberType),
        th.Property("total_available", th.NumberType),
        th.Property("total_allocated", th.NumberType),
        th.Property("total_unallocated", th.NumberType),
        th.Property("po_total_committed", th.NumberType),
        th.Property("total_mfg_ordered", th.NumberType),
        th.Property("fulfillment_type", th.StringType),
        th.Property(
            "warehouses",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.NumberType),
                    th.Property("low_stock_threshold", th.NumberType),
                    th.Property("out_of_stock_threshold", th.NumberType),
                    th.Property("location_in_warehouse", th.StringType),
                    th.Property("on_hand", th.NumberType),
                    th.Property("created", th.DateTimeType),
                    th.Property("updated", th.DateTimeType),
                    th.Property(
                        "address",
                        th.ObjectType(
                            th.Property("company", th.StringType),
                            th.Property("street1", th.StringType),
                            th.Property("street2", th.StringType),
                            th.Property("zip", th.StringType),
                            th.Property("id", th.NumberType),
                            th.Property("name", th.StringType),
                            th.Property("city", th.StringType),
                            th.Property("state", th.StringType),
                            th.Property("country", th.StringType),
                            th.Property("email", th.StringType),
                            th.Property("phone", th.StringType),
                            th.Property("fax", th.StringType),
                            th.Property("reference_number", th.StringType),
                            th.Property("cart_address_id", th.StringType),
                            th.Property(
                                "validation",
                                th.ObjectType(
                                    th.Property(
                                        "suggested",
                                        th.CustomType({"type": ["array", "string"]}),
                                    ),
                                    th.Property("is_error", th.BooleanType),
                                    th.Property("additional_text", th.StringType),
                                ),
                            ),
                        ),
                    ),
                    th.Property("cart", th.StringType),
                    th.Property("is_configured_for_shipping", th.BooleanType),
                    th.Property("is_default_location", th.BooleanType),
                    th.Property("physical_on_hand", th.NumberType),
                    th.Property("available", th.NumberType),
                    th.Property("committed", th.NumberType),
                    th.Property("allocated", th.NumberType),
                    th.Property("unallocated", th.NumberType),
                    th.Property("po_committed", th.NumberType),
                    th.Property("mfg_ordered", th.NumberType),
                )
            ),
        ),
        th.Property("kit_components", th.CustomType({"type": ["array", "string"]})),
        th.Property("carts", th.CustomType({"type": ["array", "string"]})),
        th.Property("suppliers", th.CustomType({"type": ["array", "string"]})),
        th.Property("to_be_shipped", th.NumberType),
        th.Property("available_on_hand", th.NumberType),
        th.Property("on_pos", th.NumberType),
    ).to_dict()


class SuppliersStream(OrdoroStream):
    """Define custom stream."""

    name = "supplier"
    path = "/supplier/"
    primary_keys = ["id"]
    replication_key = "updated"
    records_jsonpath = "$.supplier[*]"
    schema = th.PropertiesList(
        th.Property("_link", th.StringType),
        th.Property("id", th.NumberType),
        th.Property("company_id", th.NumberType),
        th.Property("warehouse_id", th.NumberType),
        th.Property("created", th.DateTimeType),
        th.Property("updated", th.DateTimeType),
        th.Property("archive_date", th.DateTimeType),
        th.Property("automatically_show_supplier_price", th.BooleanType),
        th.Property("dropship_prefer_cart_order_line_names", th.BooleanType),
        th.Property(
            "address",
            th.ObjectType(
                th.Property("company", th.StringType),
                th.Property("street1", th.StringType),
                th.Property("street2", th.StringType),
                th.Property("zip", th.StringType),
                th.Property("id", th.NumberType),
                th.Property("name", th.StringType),
                th.Property("city", th.StringType),
                th.Property("state", th.StringType),
                th.Property("country", th.StringType),
                th.Property("email", th.StringType),
                th.Property("phone", th.StringType),
                th.Property("fax", th.StringType),
                th.Property("reference_number", th.StringType),
                th.Property("cart_address_id", th.StringType),
                th.Property(
                    "validation", th.CustomType({"type": ["object", "string"]})
                ),
            ),
        ),
        th.Property("vendor_config", th.CustomType({"type": ["object", "string"]})),
        th.Property("default_shipping_method", th.StringType),
        th.Property(
            "shipping_method_maps", th.CustomType({"type": ["array", "string"]})
        ),
    ).to_dict()


class WarehouseStream(OrdoroStream):
    """Define custom stream."""

    name = "warehouse"
    path = "/warehouse/"
    primary_keys = ["id"]
    records_jsonpath = "$.warehouse[*]"
    replication_key = None
    schema = th.PropertiesList(
        th.Property("_link", th.StringType),
        th.Property("id", th.NumberType),
        th.Property(
            "address",
            th.ObjectType(
                th.Property("company", th.StringType),
                th.Property("street1", th.StringType),
                th.Property("street2", th.StringType),
                th.Property("zip", th.StringType),
                th.Property("id", th.NumberType),
                th.Property("name", th.StringType),
                th.Property("city", th.StringType),
                th.Property("state", th.StringType),
                th.Property("country", th.StringType),
                th.Property("email", th.StringType),
                th.Property("phone", th.StringType),
                th.Property("fax", th.StringType),
                th.Property(
                    "validation", th.CustomType({"type": ["object", "string"]})
                ),
            ),
        ),
        th.Property("cart", th.StringType),
        th.Property("is_configured_for_shipping", th.BooleanType),
        th.Property("is_default_location", th.BooleanType),
    ).to_dict()


class CompanyStream(OrdoroStream):
    """Define custom stream."""

    name = "company"
    path = "/company/"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("_link", th.StringType),
        th.Property("id", th.NumberType),
        th.Property("name", th.StringType),
        th.Property("contact", th.StringType),
        th.Property("address", th.StringType),
        th.Property("website", th.StringType),
        th.Property("phone", th.StringType),
        th.Property("fax", th.StringType),
        th.Property("email", th.StringType),
        th.Property("billing_email", th.StringType),
        th.Property("currency_symbol", th.StringType),
        th.Property("created", th.DateTimeType),
        th.Property("features", th.CustomType({"type": ["object", "string"]})),
        th.Property("stripe_customer_id", th.StringType),
        th.Property("shipper_currency", th.StringType),
        th.Property("footer", th.StringType),
        th.Property("mail_sender", th.StringType),
        th.Property("activated", th.DateTimeType),
        th.Property("deactivated", th.DateTimeType),
        th.Property("plan", th.StringType),
        th.Property("pitney_rate_type", th.StringType),
        th.Property("autosync_frequency", th.StringType),
        th.Property("app_message", th.StringType),
        th.Property("api_locked", th.BooleanType),
        th.Property("print_node", th.CustomType({"type": ["object", "string"]})),
        th.Property("vendor_connect", th.BooleanType),
        th.Property("payment_details", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()


class PurchaseOrder(OrdoroStream):
    """Define custom stream."""

    name = "purchaseorder"
    path = "/purchase_order/"
    primary_keys = ["po_id"]
    records_jsonpath = "$.purchase_order[*]"
    replication_key = "updated"
    schema = th.PropertiesList(
        th.Property("status", th.StringType),
        th.Property("company_id", th.NumberType),
        th.Property("po_id", th.StringType),
        th.Property("shipping_method", th.StringType),
        th.Property("payment_method", th.StringType),
        th.Property("instructions", th.StringType),
        th.Property("sent", th.StringType),
        th.Property("estimated_delivery_date", th.DateTimeType),
        th.Property("created", th.DateTimeType),
        th.Property("updated", th.DateTimeType),
        th.Property("shipping_amount", th.NumberType),
        th.Property("tax_amount", th.NumberType),
        th.Property("discount_amount", th.NumberType),
        th.Property("_link", th.StringType),
        th.Property(
            "items",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.NumberType),
                    th.Property("quantity", th.NumberType),
                    th.Property("unit_price", th.NumberType),
                    th.Property("discount_amount", th.NumberType),
                    th.Property("_link", th.StringType),
                    th.Property(
                        "product",
                        th.ObjectType(
                            th.Property("type", th.StringType),
                            th.Property("cost", th.NumberType),
                            th.Property("id", th.NumberType),
                            th.Property("company_id", th.NumberType),
                            th.Property("name", th.StringType),
                            th.Property("sku", th.StringType),
                            th.Property("taxable", th.StringType),
                            th.Property("weight", th.NumberType),
                            th.Property("length", th.NumberType),
                            th.Property("width", th.NumberType),
                            th.Property("height", th.NumberType),
                            th.Property("price", th.NumberType),
                            th.Property("customer_text", th.StringType),
                            th.Property("internal_notes", th.StringType),
                            th.Property("total_on_hand", th.NumberType),
                            th.Property("category", th.StringType),
                            th.Property("is_kit_parent", th.BooleanType),
                            th.Property("archive_date", th.DateTimeType),
                            th.Property("hs_code", th.StringType),
                            th.Property("country_of_origin", th.StringType),
                            th.Property("customs_description", th.StringType),
                            th.Property("created", th.DateTimeType),
                            th.Property("updated", th.DateTimeType),
                            th.Property("upc", th.StringType),
                            th.Property("asin", th.StringType),
                            th.Property("image_url", th.StringType),
                            th.Property("default_image_id", th.IntegerType),
                            th.Property("_link", th.StringType),
                            th.Property(
                                "tags", th.CustomType({"type": ["array", "string"]})
                            ),
                            th.Property("total_committed", th.NumberType),
                            th.Property("total_available", th.NumberType),
                            th.Property("total_allocated", th.NumberType),
                            th.Property("total_unallocated", th.NumberType),
                            th.Property("po_total_committed", th.NumberType),
                            th.Property("total_mfg_ordered", th.NumberType),
                            th.Property("fulfillment_type", th.StringType),
                            th.Property(
                                "suppliers",
                                th.CustomType({"type": ["object", "string"]}),
                            ),
                            th.Property(
                                "warehouse",
                                th.CustomType({"type": ["object", "string"]}),
                            ),
                            th.Property("to_be_shipped", th.NumberType),
                            th.Property("available_on_hand", th.NumberType),
                            th.Property("quantity_received", th.NumberType),
                        ),
                    ),
                )
            ),
        ),
        th.Property(
            "supplier",
            th.ObjectType(
                th.Property("_link", th.StringType),
                th.Property("id", th.NumberType),
                th.Property("company_id", th.NumberType),
                th.Property("warehouse_id", th.NumberType),
                th.Property("created", th.DateTimeType),
                th.Property("updated", th.DateTimeType),
                th.Property("archive_date", th.DateTimeType),
                th.Property("automatically_show_supplier_price", th.BooleanType),
                th.Property("dropship_prefer_cart_order_line_names", th.BooleanType),
                th.Property(
                    "address",
                    th.ObjectType(
                        th.Property("company", th.StringType),
                        th.Property("street1", th.StringType),
                        th.Property("street2", th.StringType),
                        th.Property("zip", th.StringType),
                        th.Property("id", th.NumberType),
                        th.Property("name", th.StringType),
                        th.Property("city", th.StringType),
                        th.Property("state", th.StringType),
                        th.Property("country", th.StringType),
                        th.Property("email", th.StringType),
                        th.Property("phone", th.StringType),
                        th.Property("fax", th.StringType),
                        th.Property("reference_number", th.StringType),
                        th.Property("cart_address_id", th.StringType),
                        th.Property(
                            "validation", th.CustomType({"type": ["object", "string"]})
                        ),
                    ),
                ),
                th.Property(
                    "vendor_config", th.CustomType({"type": ["object", "string"]})
                ),
                th.Property("default_shipping_method", th.StringType),
                th.Property(
                    "shipping_method_maps", th.CustomType({"type": ["array", "string"]})
                ),
            ),
        ),
        th.Property(
            "warehouse",
            th.ObjectType(
                th.Property("_link", th.StringType),
                th.Property("id", th.NumberType),
                th.Property(
                    "address",
                    th.ObjectType(
                        th.Property("company", th.StringType),
                        th.Property("street1", th.StringType),
                        th.Property("street2", th.StringType),
                        th.Property("zip", th.StringType),
                        th.Property("id", th.NumberType),
                        th.Property("name", th.StringType),
                        th.Property("city", th.StringType),
                        th.Property("state", th.StringType),
                        th.Property("country", th.StringType),
                        th.Property("email", th.StringType),
                        th.Property("phone", th.StringType),
                        th.Property("fax", th.StringType),
                        th.Property(
                            "validation", th.CustomType({"type": ["object", "string"]})
                        ),
                    ),
                ),
                th.Property("cart", th.StringType),
                th.Property("is_configured_for_shipping", th.BooleanType),
                th.Property("is_default_location", th.BooleanType),
            ),
        ),
        th.Property("comments", th.CustomType({"type": ["array", "string"]})),
        th.Property("goods_receipts", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()
