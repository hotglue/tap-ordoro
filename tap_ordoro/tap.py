"""Ordoro tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th  # JSON schema typing helpers

# TODO: Import your custom stream types here:
from tap_ordoro.streams import (
    CompanyStream,
    OrdersStream,
    OrdoroStream,
    ProductsStream,
    PurchaseOrder,
    SuppliersStream,
    WarehouseStream,
)

# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
STREAM_TYPES = [
    OrdersStream,
    ProductsStream,
    SuppliersStream,
    WarehouseStream,
    CompanyStream,
    PurchaseOrder,
]


class TapOrdoro(Tap):
    """Ordoro tap class."""

    name = "tap-ordoro"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
            required=True,
        ),
        th.Property(
            "api_url",
            th.StringType,
            default="https://api.ordoro.com",
            description="The url for the API service",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapOrdoro.cli()
